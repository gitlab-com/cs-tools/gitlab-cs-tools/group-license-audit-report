#!/usr/bin/env python3

import gitlab
import json
import csv
import argparse
from datetime import datetime
import yaml
from junit_xml import TestSuite, TestCase


def write_csv(file, group_licenses, findings):
    reportwriter = csv.writer(file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["project","project_url","dependency_name","dependency_version","dependency_package_manager","dependency_path","license_name","license_url","pipeline_url","job_url","pipeline_date"]
    reportwriter.writerow(fields)

    for project in group_licenses:
        project_licenses = licenses[project]
        #this is a map of licenses that dependencies refer to
        license_map = {}
        for l in project_licenses["licenses"]:
            license_map[l["id"]] = [l["name"], l["url"]]

        for dependency in project_licenses["dependencies"]:
            for deplicense in dependency["licenses"]:
                if deplicense in license_map:
                    row = []
                    row.append(project)
                    row.append(project_licenses["project_url"])
                    row.append(dependency["name"])
                    row.append(dependency["version"])
                    row.append(dependency["package_manager"])
                    row.append(dependency["path"])
                    row.append(license_map[deplicense][0])
                    row.append(license_map[deplicense][1])
                    row.append(project_licenses["pipeline_url"])
                    row.append(project_licenses["job_url"])
                    row.append(project_licenses["date"])
                    reportwriter.writerow(row)
                else:
                    print("%s not in licenses for project %s and dependency %s" % (deplicense, project, dependency["name"]))
                    finding = {}
                    finding["status"] = "failure"
                    finding["name"] = "%s - print csv" % project
                    finding["classname"] = project
                    finding["description"] = "Could not resolve license id %s in dependency %s." % (deplicense, dependency["name"])
                    findings[project].append(finding)

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        print("Getting projects of %s" % topgroup)
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(get_all=True, iterator=True, include_subgroups=True)
        for group_project in group_projects:
            project_object = gl.projects.get(group_project.id)
            print("Adding project %s" % project_object.attributes["path_with_namespace"])
            projects.append(project_object)
    return projects

def log_logging_results(project, found_successful_master, found_license_scanning_job, retrieved_license_scanning_artifact, checked_pipelines):
    findings = []
    if not found_successful_master:
        finding = {}
        finding["status"] = "failure"
        finding["name"] = "%s - find successful master pipeline" % project
        finding["classname"] = project
        finding["description"] = "Could not find successful master build for %s." % project
        findings.append(finding)
    else:
        finding = {}
        finding["status"] = "success"
        finding["name"] = "%s - find successful master pipeline" % project
        finding["classname"] = project
        finding["description"] = ""
        findings.append(finding)
    if not found_license_scanning_job:
        finding = {}
        finding["status"] = "failure"
        finding["name"] = "%s - find license scanning job" % project
        finding["classname"] = project
        finding["description"] = "Could not find license scanning job for %s." % project
        findings.append(finding)
    else:
        finding = {}
        finding["status"] = "success"
        finding["name"] = "%s - find license scanning job" % project
        finding["classname"] = project
        finding["description"] = ""
        findings.append(finding)
    if not retrieved_license_scanning_artifact:
        finding = {}
        finding["status"] = "failure"
        finding["name"] = "%s - download license scanning artifact" % project
        finding["classname"] = project
        finding["description"] = "Could not load license scanning report %s. You may have to expose the artifact via paths." % project
        findings.append(finding)
    else:
        finding = {}
        finding["status"] = "success"
        finding["name"] = "%s - download license scanning artifact" % project
        finding["classname"] = project
        finding["description"] = ""
        findings.append(finding)
    if checked_pipelines > 1:
        finding = {}
        finding["status"] = "failure"
        finding["name"] = "%s - ensure master pipeline is latest" % project
        finding["classname"] = project
        finding["description"] = "Latest master broken in %s." % project
        findings.append(finding)
    else:
        finding = {}
        finding["status"] = "success"
        finding["name"] = "%s - ensure master pipeline is latest" % project
        finding["classname"] = project
        finding["description"] = ""
        findings.append(finding)
    return findings

def make_test_suite(findings):
    test_cases = []
    for project in findings.values():
        for finding in project:
            name = finding["name"]
            classname = finding["classname"]
            case = TestCase(name, classname)
            if finding["status"] == "failure":
                case.add_failure_info(finding["description"])
                case.status = "fail"
            test_cases.append(case)
    return TestSuite("License report tests", test_cases)

def get_license_reports(projects, findings):
    licenses = {}
    for project in projects:
        found_successful_master = False
        found_license_scanning_job = False
        retrieved_license_scanning_artifact = False
        checked_pipelines = 0

        pipelines = None
        try:
            pipelines = project.pipelines.list(ref=project.default_branch, get_all=True)
        except:
            print("Can not access project pipelines for %s" % project.attributes["path_with_namespace"])
            findings[project.attributes["path_with_namespace"]] = log_logging_results(project.attributes["path_with_namespace"], found_successful_master, found_license_scanning_job, retrieved_license_scanning_artifact, checked_pipelines)
            continue
        print("Checking pipelines of %s for license_scanning jobs." % project.attributes["path_with_namespace"])

        for pipeline in pipelines:
            checked_pipelines += 1
            if pipeline.attributes["status"] == "success":
                found_successful_master = True
            jobs = pipeline.jobs.list(get_all=True)
            for job in jobs:
                for artifact in job.attributes["artifacts"]:
                    if "license" in artifact["file_type"]:
                        found_license_scanning_job = True
                        job_object = project.jobs.get(job.id)
                        try:
                            license_json = json.loads(job_object.artifact(artifact["filename"]))
                            license_json["date"] = job_object.attributes["created_at"]
                            license_json["pipeline_id"] = pipeline.attributes["id"]
                            license_json["pipeline_url"] = pipeline.attributes["web_url"]
                            license_json["job_id"] = job_object.attributes["id"]
                            license_json["job_url"] = job_object.attributes["web_url"]
                            license_json["project_url"] = project.attributes["web_url"]
                            licenses[project.attributes["path_with_namespace"]] = license_json
                            retrieved_license_scanning_artifact = True
                        except:
                            print("Could not pull license artifact: %s %s %s" % (project.attributes["path_with_namespace"], str(pipeline.id), str(job.id)))
            if found_successful_master:
                break
        findings[project.attributes["path_with_namespace"]] = log_logging_results(project.attributes["path_with_namespace"], found_successful_master, found_license_scanning_job, retrieved_license_scanning_artifact, checked_pipelines)
    return licenses

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='CSV file that defines requested projects')
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token, retry_transient_errors=True)

reportdate = datetime.now().strftime('%Y-%m-%d')

configfile = args.configfile
groups = []
projects = []
fields = []
with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    gitlaburl = config["gitlab_url"] if config["gitlab_url"].endswith("/") else config["gitlab_url"] + "/"
    gl = gitlab.Gitlab(gitlaburl, private_token=args.token, retry_transient_errors=True)

    if "groups" in config:
        groups = config["groups"]
        if not groups:
            print("Groups list is empty, please specify a group or comment out the groups field.")
            exit(1)
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Error: No group or project configured. Stopping.")
            exit(1)
        else:
            projects_paths = config["projects"]
            for project in projects_paths:
                projects.append(gl.projects.get(project))

findings = {}
licenses = get_license_reports(projects, findings)

with open("license_report_%s.json" % reportdate , "w") as json_report:
    json.dump(licenses, json_report, indent=4)

with open("license_report_%s.csv" % reportdate , "w") as csv_report:
    write_csv(csv_report, licenses, findings)

# produce a combined json of all found licenses in specified groups/projects
if licenses:
    combined_licenses = {
        "licenses": [],
        "dependencies": [],
        "version": list(licenses.values())[0]["version"]
    }
    for key,value in licenses.items():
        for license in value["licenses"]:
            existing_license = next(filter(lambda l: l["id"] == license["id"], combined_licenses["licenses"]), None)
            if existing_license is None:
                combined_licenses["licenses"].append(license)
        for dependency in value["dependencies"]:
            existing_dependency = next(filter(lambda d: d["name"] == dependency["name"] and d["version"] == dependency["version"] and d["package_manager"] == dependency["package_manager"], \
                combined_licenses["dependencies"]), None)
            if existing_dependency is None:
                combined_licenses["dependencies"].append(dependency)

    with open("gl-license-scanning-report.json", "w") as json_report:
        json.dump(combined_licenses, json_report, indent=4)

tests = make_test_suite(findings)
with open("license_report_tests.xml","w") as testfile:
    testfile.write(TestSuite.to_xml_string([tests]))
